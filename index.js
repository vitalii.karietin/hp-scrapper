const path = require("path");
const http = require('http');
const puppeteer = require('puppeteer');
const fs = require('fs');
const Apify = require('apify');
const csvParse = require('@fast-csv/parse');
const request = require('request');

const CsvFile = require('./CsvFile').CsvFile;


const sleep = m => new Promise(r => setTimeout(r, m));

Apify.main(async () => {
    const csvFile = new CsvFile({
        path: path.resolve(__dirname, 'hockey.csv'),
        headers: ['index', 'name', 'date_of_birth', 'age', 'place_of_birth', 'nation', 'youth_team', 'position', 'height', 'weight', 'shoots', 'contract', "picture", "extended_data", 'player_description'],
    });

    const browser = await Apify.launchPuppeteer({
        headless: true,
        stealth: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--disable-gpu',
            '--window-size=1920x1080',
        ]
    });
    const page = await browser.newPage();
    await page.setRequestInterception(true);
    page.on('request', request => {
        if (
            request.resourceType() === 'script' ||
            request.resourceType() === 'stylesheet' ||
            request.resourceType() === 'font' ||
            request.resourceType() === 'eventsource' ||
            (request.resourceType() === 'image' && !request.url().includes('eliteprospects.com/layout/players'))
        )
            request.abort();
        else
            request.continue();
    });

    // let lastRow = null;
    //
    // if(fs.existsSync('./hockey.csv')) {
    //     await fs.createReadStream(path.resolve(__dirname, 'hockey.csv'))
    //         .pipe(csvParse.parse())
    //         .on('error', error => lastRow = 0)
    //         .on('data', data => {lastRow = data[0]; return data;})
    //         // .on('end', rowCount => {console.log(rowCount)})
    // }

    for (let i = 200001; i < 250001; i++) {
        try {
            const randomSleep = Math.floor(Math.random() * 5000) + 2000;
            await sleep(randomSleep);
            console.log(`Delay - ${Math.floor(randomSleep / 1000)}s`);
            console.log(`https://www.eliteprospects.com/player/${i}/jack-hughes`);
            await page.goto(`https://www.eliteprospects.com/player/${i}/jack-hughes`, {
                timeout: 25000,
                waitUntil: 'networkidle2',
                referer: 'https://www.google.ca/'
            });

            //Player name
            await page.waitForSelector('.plytitle', {
                timeout: 1000
            })
                .catch(e => {
                    console.error('PLAYER NAME - ', e);
                });
            const name = await page.$eval('.plytitle', node => [...node.childNodes].filter(e => e.nodeType === 3).map(e => e.textContent).map(t => t.trim()).filter(e => e !== '')[0]);

            //Player picture
            let player_picture = '';

            try {
                await page.waitForSelector('div.profile-picture', {
                    timeout: 1000
                });
                player_picture = await page.$eval('div.profile-picture img', node => node.src ? node.src : "");
            } catch (e) {
                console.error('PLAYER PICTURE - ', e)
            }

            //General player info
            await page.waitForSelector('div.table-view', {
                timeout: 2000
            })
                .catch(e => {
                    console.error('PLAYER INFO - ', e);
                });
            const player_info = await page.$eval('.table-view', node => {
                const keys = [...node.querySelectorAll('.table-view div.row ul li div:first-child')].map(item => item.textContent.trim()).map(key => key.trim().replace(/\s+/g, '_').toLowerCase());
                const values = [...node.querySelectorAll('.table-view div.row ul li div:nth-child(2)')].map((item, index) => {
                    return keys[index] === 'nation' ?
                        item.textContent.split('/').map(e => e.trim()).join(' / ')
                        :
                        item.textContent.trim()
                });
                const additionalData = [...node.querySelectorAll('.plyr_details .table-view > div:nth-child(2) ul li')].map( item => {
                    if(item) {
                        const key = item.querySelector('div:first-child').textContent.trim();
                        let value;
                        if([...item.querySelector('div:nth-child(2)').childNodes].find(i => i.classList && i.classList.contains('career-highlight') ? i : null)) {
                            const filteredChildren = [...item.querySelector('div:nth-child(2)').childNodes].filter( i => {
                                if(i.classList && i.classList.contains('career-highlight'))
                                    return i;
                            });
                            value = filteredChildren.map( i => {
                                const image = i.querySelector('a img').src;
                                const link = i.querySelector('a').href;
                                const count = i.querySelector('span.caption').textContent.trim();
                                return {image, link, count}
                            })
                        } else {
                            value = item.querySelector('div:nth-child(2)').textContent.trim().split('\n')[0]
                        }
                        return {key, value};
                    }
                }).filter( item => item.key.toLowerCase() !== 'agency');

                return {keys, values, additionalData}
            });
            let player_description = '';
            try {
                await page.waitForSelector('.table-view + .dtl-txt',{
                    timeout: 1000
                });
                player_description = await page.$eval('.table-view + .dtl-txt', node => node.innerHTML.trim().replace('/(\\r\\n|\\n|\\r)/gm', ''));
            } catch (e) {
                console.error('PLAYER DESCRIPTION - ', e)
            }

            const formattedPlayerInfo = player_info.keys.reduce((o, k, i) => ({...o, [k]: player_info.values[i]}), {});

            try {
                if(fs.existsSync(path.resolve(__dirname, 'hockey.csv'))) {
                    await csvFile.append([{
                        index: i,
                        name: name,
                        picture: player_picture,
                        ...formattedPlayerInfo,
                        extended_data: JSON.stringify(player_info.additionalData),
                        player_description: player_description
                    }])
                } else {
                    await csvFile.create([{
                        index: i,
                        name: name,
                        picture: player_picture,
                        ...formattedPlayerInfo,
                        extended_data: JSON.stringify(player_info.additionalData),
                        player_description: player_description
                    }]);
                }
            } catch (e) {
                console.error(e)
            }
            
        } catch (e) {
            await sleep(60000);
            console.log('oshibka blyat\'\n', e);
        }

    }
});

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});